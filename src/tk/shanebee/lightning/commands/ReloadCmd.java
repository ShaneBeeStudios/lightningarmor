package tk.shanebee.lightning.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import tk.shanebee.lightning.LightningArmor;
import tk.shanebee.lightning.utils.Utils;

public class ReloadCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		LightningArmor.getPlugin().getConfigFile().loadConfig(sender);
		LightningArmor.getPlugin().createNewPlayerTask();
		Utils.sendPluginMessage(sender, "- Config reloaded!");
		return true;
	}

}
