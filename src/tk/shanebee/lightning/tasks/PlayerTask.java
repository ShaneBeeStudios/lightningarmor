package tk.shanebee.lightning.tasks;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tk.shanebee.lightning.LightningArmor;

import java.util.List;
import java.util.Random;

public class PlayerTask {

	private int chance;
	private int item_chance;
	private int interval;
	private boolean safe_shelter;
	private boolean include_armor;
	private boolean hand_only;
	private List<Material> materials;

	public PlayerTask() {
		this.chance = LightningArmor.getPlugin().getConfigFile().getChance();
		this.item_chance = LightningArmor.getPlugin().getConfigFile().getItemChance();
		this.interval = LightningArmor.getPlugin().getConfigFile().getInterval();
		this.materials = LightningArmor.getPlugin().getConfigFile().getMaterials();
		this.safe_shelter = LightningArmor.getPlugin().getConfigFile().getShelter();
		this.hand_only = LightningArmor.getPlugin().getConfigFile().getHandOnly();
		this.include_armor = LightningArmor.getPlugin().getConfigFile().getIncludeArmor();

		startLightningTask();
	}

	private void startLightningTask() {
		Random random = new Random();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(LightningArmor.getPlugin(), () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (random.nextInt(100) <= chance) {
					if (isStormy(player) && isAboveSeaLevel(player) && isNotSafe(player))
						strikePlayer(player);
				}
			}
		}, 20 * interval, 20 * interval);
	}

	private void strikePlayer(Player player) {
		int i = 0;
		int random = new Random().nextInt(item_chance) + 1;
		if (hand_only) {
			if (player.getInventory().getItemInMainHand() != null) {
				if (materials.contains(player.getInventory().getItemInMainHand().getType())) {
					i++;
				}
			}
			if (player.getInventory().getItemInOffHand() != null) {
				if (materials.contains(player.getInventory().getItemInOffHand().getType())) {
					i++;
				}
			}
		} else {
			for (ItemStack item : player.getInventory().getStorageContents()) {
				if (item != null && materials.contains(item.getType())) {
					i++;
				}
			}
		}
		if (include_armor) {
			for (ItemStack item : player.getInventory().getArmorContents()) {
				if (item != null && materials.contains(item.getType())) {
					i++;
				}
			}
		}
		if (random <= i) {
			player.getLocation().getWorld().strikeLightning(player.getLocation());
		}
	}

	private boolean isStormy(Player player) {
		return player.getWorld().hasStorm() && player.getWorld().isThundering();
	}

	private boolean isNotSafe(Player player) {
		if (player.hasPermission("lightningarmor.bypass")) return false;
		Location loc = player.getLocation();
		GameMode mode = player.getGameMode();
		if (mode == GameMode.CREATIVE || mode == GameMode.SPECTATOR) return false;
		if (safe_shelter) {
			return loc.getY() >= loc.getWorld().getHighestBlockYAt(loc);
		} else {
			return true;
		}
	}

	private boolean isAboveSeaLevel(Player player) {
		Location location = player.getLocation();
		return location.getY() >= location.getWorld().getSeaLevel();
	}

}
