package tk.shanebee.lightning;

import org.bukkit.plugin.java.JavaPlugin;
import tk.shanebee.lightning.commands.ReloadCmd;
import tk.shanebee.lightning.data.Config;
import tk.shanebee.lightning.tasks.PlayerTask;
import tk.shanebee.lightning.utils.Utils;

public class LightningArmor extends JavaPlugin {

	private Config config;
	private static LightningArmor plugin;
	@SuppressWarnings("unused")
	private PlayerTask playerTask;

	@Override
	public void onEnable() {
		Utils.log("Enabling LightningArmor");
		plugin = this;
		this.config = new Config(this);

		this.playerTask = new PlayerTask();
		getCommand("lightningarmorreload").setExecutor(new ReloadCmd());

		Utils.log("Successfully enabled LightningArmor");
	}

	@Override
	public void onDisable() {
		Utils.log("Successfully disabled LightningArmor");
	}

	public Config getConfigFile() {
		return this.config;
	}

	public static LightningArmor getPlugin() {
		return plugin;
	}

	public void createNewPlayerTask() {
		this.playerTask = new PlayerTask();
	}

}
