package tk.shanebee.lightning.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Utils {

	private static String prefix = "&7[&bLightning&3Armor&7] ";

	public static void scm(CommandSender player, String message) {
		player.sendMessage(getColString(message));
	}

	public static void sendPluginMessage(CommandSender player, String message) {
		scm(player, prefix + message);
	}

	public static void log(String message) {
		Bukkit.getConsoleSender().sendMessage(getColString(prefix + "- " + message));
	}

	public static void warn(String message) {
		Bukkit.getConsoleSender().sendMessage(getColString(prefix + "- &e" + message));
	}

	public static String getColString(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

}
