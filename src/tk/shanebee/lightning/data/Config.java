package tk.shanebee.lightning.data;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import tk.shanebee.lightning.LightningArmor;
import tk.shanebee.lightning.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
public class Config {

	private LightningArmor plugin;
	private FileConfiguration config;

	private List<Material> materials;
	private int interval;
	private int chance;
	private int item_chance;
	private boolean safe_shelter;
	private boolean hand_only;
	private boolean include_armor;

	public Config(LightningArmor plugin) {
		this.plugin = plugin;
		loadConfig(plugin.getServer().getConsoleSender());
	}

	public void loadConfig(CommandSender sender) {
		Utils.sendPluginMessage(sender,"- Loading config");
		loadFile();

		materials = new ArrayList<>();
		List<String> nullItems = new ArrayList<>();
		for (String string : config.getStringList("settings.metallic-items")) {
			if (stringToMaterial(string) != null) {
				materials.add(stringToMaterial(string));
			} else {
				nullItems.add(string);
			}
		}

		this.chance = config.getInt("settings.strike-lightning.chance");
		this.interval = config.getInt("settings.strike-lightning.interval");
		this.item_chance = config.getInt("settings.strike-lightning.item-chance");
		this.safe_shelter = config.getBoolean("settings.strike-lightning.safe-under-shelter");
		this.hand_only = config.getBoolean("settings.inventory.hand-only");
		this.include_armor = config.getBoolean("settings.inventory.include-armor");

		if (nullItems.size() > 0) {
			Utils.sendPluginMessage(sender,"- &eCould not load the following &b" + nullItems.size() + " &eitems:");
			for (String item : nullItems) {
				Utils.sendPluginMessage(sender, "  - &c" + item);
			}
		}
		Utils.sendPluginMessage(sender, "- Successfully loaded config with &b" + materials.size() + "&7 metallic items");
	}

	private void loadFile() {
		File configFile = new File(plugin.getDataFolder(), "config.yml");
		if (!configFile.exists()) {
			plugin.saveResource("config.yml", false);
			Utils.log("New config.yml file &acreated!");
		}
		config = YamlConfiguration.loadConfiguration(configFile);
	}

	private Material stringToMaterial(String string) {
		try {
			return Material.valueOf(string);
		} catch (Exception ignore) {
			return null;
		}
	}

	public List<Material> getMaterials() {
		return this.materials;
	}

	public int getChance() {
		return this.chance;
	}

	public int getInterval() {
		return this.interval;
	}

	public int getItemChance() {
		return this.item_chance;
	}

	public boolean getShelter() {
		return this.safe_shelter;
	}

	public boolean getHandOnly() {
		return this.hand_only;
	}

	public boolean getIncludeArmor() {
		return this.include_armor;
	}

}
